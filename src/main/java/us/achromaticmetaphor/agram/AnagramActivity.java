package us.achromaticmetaphor.agram;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import java.text.Normalizer;

public class AnagramActivity extends Activity {
  AnagramFragment agramFragment;
  Generator generator;

  private void refreshWith(EditText edit, boolean longMode) {
      agramFragment.input = Normalizer.normalize(edit.getText().toString(), Normalizer.Form.NFC);
      agramFragment.longMode = longMode;
      agramFragment.refresh();
  }

  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_anagram);
    generator = (Generator) getIntent().getExtras().getSerializable("generator");
    agramFragment = (AnagramFragment) getFragmentManager().findFragmentById(R.id.agramFragment);
    chooseCharacters();
  }

  public void chooseCharacters() {
    agramFragment.gen = generator;
    final EditText edit = new EditText(this);
    edit.setText(agramFragment.input);
    edit.selectAll();
    AlertDialog.Builder builder =
        new AlertDialog.Builder(this)
            .setView(edit)
            .setTitle(generator.inputPrompt())
            .setPositiveButton(generator.shortLabel(), (DialogInterface di, int button) -> { refreshWith(edit, false); })
            .setNegativeButton("Cancel", null);
    if (generator.hasLongMode())
      builder.setNeutralButton(generator.longLabel(), (DialogInterface di, int button) -> { refreshWith(edit, true); });
    builder.show();
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    final int itemId = item.getItemId();
    if (itemId == R.id.share)
      agramFragment.share();
    else if (itemId == R.id.chooseCharacters)
      chooseCharacters();
    else if (itemId == R.id.refresh)
      agramFragment.refresh();
    return true;
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.anagrams, menu);
    return super.onCreateOptionsMenu(menu);
  }
}
