package us.achromaticmetaphor.agram;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class AnagramFragment
    extends Fragment implements AbsListView.OnScrollListener {

  private static final int SCROLL_BY = 128;

  Generator gen;
  String input = "";
  ArrayList<String> shareList;
  boolean longMode = false;
  private ArrayAdapter adapter;
  private ProgressDialog pdia = null;
  private boolean filling = true;
  private ListView cmdlist;

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    cmdlist = (ListView) inflater.inflate(R.layout.activity_listview, container, false);
    if (input == null)
      input = "";
    if (shareList == null) {
      shareList = new ArrayList<>();
    } else {
      ArrayAdapter adapter =
          new MonoAdapter(getActivity(),
                          android.R.layout.simple_list_item_1, shareList);
      cmdlist.setAdapter(adapter);
      cmdlist.setOnScrollListener(this);
    }
    return cmdlist;
  }

  protected void dismissDialog() {
    getActivity().runOnUiThread(() -> {
      if (pdia != null)
        pdia.dismiss();
      pdia = null;
    });
  }

  protected void initGen() {
    new Thread(() -> {
      gen.init(input, longMode);
      dismissDialog();
      scroll();
    }).start();
  }

  protected void scroll() {
    new Thread(() -> {
      updateOutput(gen.generate(SCROLL_BY));
      filling = false;
    }).start();
  }

  protected void updateOutput(ArrayList<String> data) {
    getActivity().runOnUiThread(() -> {
      shareList.addAll(data);
      adapter.notifyDataSetChanged();
    });
  }

  void share() {
    StringBuilder sb = new StringBuilder();
    for (String s : shareList)
      if (sb.length() >= 102400)
        break;
      else
        sb.append(s).append("\n");
    Intent send = new Intent();
    send.setAction(Intent.ACTION_SEND);
    send.putExtra(Intent.EXTRA_TEXT, sb.toString());
    send.setType("text/plain");
    startActivity(Intent.createChooser(send, null));
  }

  public void refresh() {
    filling = true;
    pdia = ProgressDialog.show(getActivity(), "Generating",
                               "Please wait", true, false);
    shareList = new ArrayList<>();
    adapter = new MonoAdapter(getActivity(),
                              android.R.layout.simple_list_item_1, shareList);
    cmdlist.setAdapter(adapter);
    cmdlist.setOnScrollListener(this);
    initGen();
  }

  public void onScrollStateChanged(AbsListView v, int state) {}

  public void onScroll(AbsListView v, int first, int visible, int total) {
    if (!filling)
      if (total - (first + visible) < (SCROLL_BY / 4)) {
        filling = true;
        scroll();
      }
  }

  private static class MonoAdapter extends ArrayAdapter<String> {

    MonoAdapter(Context c, int tid, List<String> lst) { super(c, tid, lst); }

    @Override
    public View getView(int pos, View cv, ViewGroup p) {
      View v = super.getView(pos, cv, p);
      ((TextView) v.findViewById(android.R.id.text1))
          .setTypeface(Typeface.MONOSPACE);
      return v;
    }
  }
}
